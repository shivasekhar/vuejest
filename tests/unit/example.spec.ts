import { shallowMount } from '@vue/test-utils'
import HelloWorld from '@/components/HelloWorld.vue'
// @ts-ignore
import {NsIcon} from '@nuskin/ns-icon'

describe('HelloWorld.vue', () => {
  it('renders props.msg when passed', () => {
    console.log('t', NsIcon);
    const msg = 'new message'
    const wrapper = shallowMount(HelloWorld, {
      propsData: { msg }
    })
    expect(wrapper.text()).toMatch(msg)
  })
})
